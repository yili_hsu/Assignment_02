var score = 0;
var topscore = 0;
var bestname = " ";
var bestscore = 0;
var name = " ";
var postsRef = '';
var states = {};
var audioJSON = {
    spritemap: {
        'part1': {
            start: 1,
            end: 100,
            loop: true,
        }
    }
};
states.end = {
    create: function () {
        bg1 = game.add.image(0, 0, 'background');
        bg2 = game.add.image(0, -bg1.height, 'background');
        var over = game.add.text(game.world.centerX, game.world.centerY - 100, 'GAME OVER', {
            font: '32px FC',
            align: 'center',
            fill: '#ff2600'
        })
        over.anchor = {
            x: 0.5,
            y: 0.5
        }
        var progressText = game.add.text(game.world.centerX, game.world.centerY - 50, 'TAP TO RESTART', {
            font: '18px FC',
            align: 'center',
            fill: '#ffffff'
        })
        progressText.anchor = {
            x: 0.5,
            y: 0.5
        }
        progressText.inputEnabled = true;
        progressText.events.onInputDown.add(function () {
            score = 0;
            game.state.start('main')
        }, this)
        this.createText();
        this.KeyboardText();
        postsRef = firebase.database().ref('users');
        postsRef.on('value', function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                if (childSnapshot.child('score').val() >= bestscore) {
                    bestscore = childSnapshot.child('score').val();
                    bestname = childSnapshot.child('name').val();
                    console.log(bestscore, bestname);
                } else {
                }
            });
        });

    },
    update: function () {
        this.usernameText.text = name;
        this.topscoreNumText.text = bestname + " " + bestscore;
    },
    createText: function () {
        this.scoreText = game.add.text(game.world.centerX - 110, game.world.centerY - 10, 'YOUR SCORE', {
            font: '16px FC',
            align: 'center',
            fill: '#66ffff'
        })
        this.scoreNumText = game.add.text(game.world.centerX + 80, game.world.centerY - 10, '0', {
            font: '16px FC',
            fill: '#ffffff'
        })
        this.scoreNumText.text = score;
        this.topscoreText = game.add.text(game.world.centerX - 100, game.world.centerY - 30, 'TOP SCORE', {
            font: '16px FC',
            fill: '#ffff33'
        })
        this.topscoreNumText = game.add.text(game.world.centerX + 50, game.world.centerY - 30, '0', {
            font: '16px FC',
            align: 'center',
            fill: '#ffffff'
        })
        this.topscoreNumText.text = bestname + bestscore;
        this.NameText = game.add.text(game.world.centerX - 100, game.world.centerY + 15, 'NAME', {
            font: '16px FC',
            align: 'center',
            fill: '#ffff33'
        })
        this.usernameText = game.add.text(game.world.centerX, game.world.centerY + 15, ' ', {
            font: '16px FC',
            align: 'center',
            fill: '#ffffff'
        })
    },
    KeyboardText: function () {
        var label_Q = game.add.text(game.world.centerX - 110, game.world.centerY + 70, 'Q', {
            font: '10px FC',
            fill: '#fff'
        });
        label_Q.anchor = {
            x: 0,
            y: 0
        }
        label_Q.inputEnabled = true;
        label_Q.events.onInputUp.add(function () {
            name += 'Q';
        });
        var label_W = game.add.text(game.world.centerX - 90, game.world.centerY + 70, 'W', {
            font: '10px FC',
            fill: '#fff'
        });
        label_W.anchor = {
            x: 0,
            y: 0
        }
        label_W.inputEnabled = true;
        label_W.events.onInputUp.add(function () {
            name += 'W';
        });
        var label_E = game.add.text(game.world.centerX - 70, game.world.centerY + 70, 'E', {
            font: '10px FC',
            fill: '#fff'
        });
        label_E.anchor = {
            x: 0,
            y: 0
        }
        label_E.inputEnabled = true;
        label_E.events.onInputUp.add(function () {
            name += 'E';
        });
        var label_R = game.add.text(game.world.centerX - 50, game.world.centerY + 70, 'R', {
            font: '10px FC',
            fill: '#fff'
        });
        label_R.anchor = {
            x: 0,
            y: 0
        }
        label_R.inputEnabled = true;
        label_R.events.onInputUp.add(function () {
            name += 'R';
        });
        var label_T = game.add.text(game.world.centerX - 30, game.world.centerY + 70, 'T', {
            font: '10px FC',
            fill: '#fff'
        });
        label_T.anchor = {
            x: 0,
            y: 0
        }
        label_T.inputEnabled = true;
        label_T.events.onInputUp.add(function () {
            name += 'T';
        });
        var label_Y = game.add.text(game.world.centerX - 10, game.world.centerY + 70, 'Y', {
            font: '10px FC',
            fill: '#fff'
        });
        label_Y.anchor = {
            x: 0,
            y: 0
        }
        label_Y.inputEnabled = true;
        label_Y.events.onInputUp.add(function () {
            name += 'Y';
        });
        var label_U = game.add.text(game.world.centerX + 10, game.world.centerY + 70, 'U', {
            font: '10px FC',
            fill: '#fff'
        });
        label_U.anchor = {
            x: 0,
            y: 0
        }
        label_U.inputEnabled = true;
        label_U.events.onInputUp.add(function () {
            name += 'U';
        });
        var label_I = game.add.text(game.world.centerX + 30, game.world.centerY + 70, 'I', {
            font: '10px FC',
            fill: '#fff'
        });
        label_I.anchor = {
            x: 0,
            y: 0
        }
        label_I.inputEnabled = true;
        label_I.events.onInputUp.add(function () {
            name += 'I';
        });
        var label_O = game.add.text(game.world.centerX + 50, game.world.centerY + 70, 'O', {
            font: '10px FC',
            fill: '#fff'
        });
        label_O.anchor = {
            x: 0,
            y: 0
        }
        label_O.inputEnabled = true;
        label_O.events.onInputUp.add(function () {
            name += 'O';
        });
        var label_P = game.add.text(game.world.centerX + 70, game.world.centerY + 70, 'P', {
            font: '10px FC',
            fill: '#fff'
        });
        label_P.anchor = {
            x: 0,
            y: 0
        }
        label_P.inputEnabled = true;
        label_P.events.onInputUp.add(function () {
            name += 'P';
        });
        ///
        var label_A = game.add.text(game.world.centerX - 110, game.world.centerY + 90, 'A', {
            font: '10px FC',
            fill: '#fff'
        });
        label_A.anchor = {
            x: 0,
            y: 0
        }
        label_A.inputEnabled = true;
        label_A.events.onInputUp.add(function () {
            name += 'A';
        });
        var label_S = game.add.text(game.world.centerX - 90, game.world.centerY + 90, 'S', {
            font: '10px FC',
            fill: '#fff'
        });
        label_S.anchor = {
            x: 0,
            y: 0
        }
        label_S.inputEnabled = true;
        label_S.events.onInputUp.add(function () {
            name += 'S';
        });
        var label_D = game.add.text(game.world.centerX - 70, game.world.centerY + 90, 'D', {
            font: '10px FC',
            fill: '#fff'
        });
        label_D.anchor = {
            x: 0,
            y: 0
        }
        label_D.inputEnabled = true;
        label_D.events.onInputUp.add(function () {
            name += 'D';
        });
        var label_F = game.add.text(game.world.centerX - 50, game.world.centerY + 90, 'F', {
            font: '10px FC',
            fill: '#fff'
        });
        label_F.anchor = {
            x: 0,
            y: 0
        }
        label_F.inputEnabled = true;
        label_F.events.onInputUp.add(function () {
            name += 'F';
        });
        var label_G = game.add.text(game.world.centerX - 30, game.world.centerY + 90, 'G', {
            font: '10px FC',
            fill: '#fff'
        });
        label_G.anchor = {
            x: 0,
            y: 0
        }
        label_G.inputEnabled = true;
        label_G.events.onInputUp.add(function () {
            name += 'G';
        });
        var label_H = game.add.text(game.world.centerX - 10, game.world.centerY + 90, 'H', {
            font: '10px FC',
            fill: '#fff'
        });
        label_H.anchor = {
            x: 0,
            y: 0
        }
        label_H.inputEnabled = true;
        label_H.events.onInputUp.add(function () {
            name += 'H';
        });
        var label_J = game.add.text(game.world.centerX + 10, game.world.centerY + 90, 'J', {
            font: '10px FC',
            fill: '#fff'
        });
        label_J.anchor = {
            x: 0,
            y: 0
        }
        label_J.inputEnabled = true;
        label_J.events.onInputUp.add(function () {
            name += 'J';
        });
        var label_K = game.add.text(game.world.centerX + 30, game.world.centerY + 90, 'K', {
            font: '10px FC',
            fill: '#fff'
        });
        label_K.anchor = {
            x: 0,
            y: 0
        }
        label_K.inputEnabled = true;
        label_K.events.onInputUp.add(function () {
            name += 'K';
        });
        var label_L = game.add.text(game.world.centerX + 50, game.world.centerY + 90, 'L', {
            font: '10px FC',
            fill: '#fff'
        });
        label_L.anchor = {
            x: 0,
            y: 0
        }
        label_L.inputEnabled = true;
        label_L.events.onInputUp.add(function () {
            name += 'L';
        });
        ///
        var label_Z = game.add.text(game.world.centerX - 110, game.world.centerY + 110, 'Z', {
            font: '10px FC',
            fill: '#fff'
        });
        label_Z.anchor = {
            x: 0,
            y: 0
        }
        label_Z.inputEnabled = true;
        label_Z.events.onInputUp.add(function () {
            name += 'Z';
        });
        var label_X = game.add.text(game.world.centerX - 90, game.world.centerY + 110, 'X', {
            font: '10px FC',
            fill: '#fff'
        });
        label_X.anchor = {
            x: 0,
            y: 0
        }
        label_X.inputEnabled = true;
        label_X.events.onInputUp.add(function () {
            name += 'X';
        });
        var label_C = game.add.text(game.world.centerX - 70, game.world.centerY + 110, 'C', {
            font: '10px FC',
            fill: '#fff'
        });
        label_C.anchor = {
            x: 0,
            y: 0
        }
        label_C.inputEnabled = true;
        label_C.events.onInputUp.add(function () {
            name += 'C';
        });
        var label_V = game.add.text(game.world.centerX - 50, game.world.centerY + 110, 'V', {
            font: '10px FC',
            fill: '#fff'
        });
        label_V.anchor = {
            x: 0,
            y: 0
        }
        label_V.inputEnabled = true;
        label_V.events.onInputUp.add(function () {
            name += 'V';
        });
        var label_B = game.add.text(game.world.centerX - 30, game.world.centerY + 110, 'B', {
            font: '10px FC',
            fill: '#fff'
        });
        label_B.anchor = {
            x: 0,
            y: 0
        }
        label_B.inputEnabled = true;
        label_B.events.onInputUp.add(function () {
            name += 'B';
        });
        var label_N = game.add.text(game.world.centerX - 10, game.world.centerY + 110, 'N', {
            font: '10px FC',
            fill: '#fff'
        });
        label_N.anchor = {
            x: 0,
            y: 0
        }
        label_N.inputEnabled = true;
        label_N.events.onInputUp.add(function () {
            name += 'N';
        });
        var label_M = game.add.text(game.world.centerX + 10, game.world.centerY + 110, 'M', {
            font: '10px FC',
            fill: '#fff'
        });
        label_M.anchor = {
            x: 0,
            y: 0
        }
        label_M.inputEnabled = true;
        label_M.events.onInputUp.add(function () {
            name += 'M';
        });
        var label_ok = game.add.text(game.world.centerX + 60, game.world.centerY + 110, 'ok', {
            font: '10px FC',
            fill: '#fff'
        });
        label_ok.anchor = {
            x: 0,
            y: 0
        }
        label_ok.inputEnabled = true;
        label_ok.events.onInputUp.add(function () {
            if (name != "") {
                console.log('ok')
                this.scoreNumText.text = name+" "+score;
                firebase.database().ref('users/' + name).set({
                    name: name,
                    score: score
                }).then(function () {
                    console.log("create successfully");
                }).catch(function (err) {
                    console.log("error");
                })
            }
        },this);
    }
}
states.start = {
    preload: function () {
        game.load.image('background', 'image/bg.png');
        game.load.image('playershot', 'image/playershot.png');
        game.load.image('pixel', 'image/pixel.png');
        game.load.image('pixel2', 'image/pixel2.png');
        game.load.image('pixel3', 'image/pixel3.png');
        game.load.image('enemyshot', 'image/enemyshot.png');
        game.load.image('littleshot', 'image/littleshot.png');
        game.load.image('littlehelper', 'image/littlehelper.png');
        game.load.image('score', 'image/score.png');
        game.load.image('player', 'image/player.png');
        game.load.image('audioon', 'image/audioon.png');
        game.load.image('audiooff', 'image/audiooff.png');
        game.load.image('heart', 'image/heart.jpg');
        /// Load block spritesheet.
        game.load.spritesheet('enemy1', 'image/enemy1.png', 212, 152);
        game.load.spritesheet('enemy2', 'image/enemy2.png', 212, 152);
        game.load.spritesheet('enemy3', 'image/enemy3.png', 212, 152);
        game.load.spritesheet('explode', 'image/explode.png', 16, 16);
        game.load.spritesheet('explode_player', 'image/explode_player.png', 157, 160);
        game.load.audio('bg', ['bgm.mp3']);
        game.load.audio('shoot', 'shoot.wav');
    },
    create: function () {
        bg1 = game.add.image(0, 0, 'background');
        bg2 = game.add.image(0, -bg1.height, 'background');
        var progressText = game.add.text(game.world.centerX, game.world.centerY, ' TAP TO START', {
            font: '32px FC',
            align: 'center',
            fill: '#ffffff'
        })
        progressText.anchor = {
            x: 0.5,
            y: 0.5
        }
        progressText.inputEnabled = true;
        this.music = game.add.audio('bg');
        this.music.play();
        progressText.events.onInputDown.add(function () {
            this.music.mute = true;
            game.state.start('main')
        }, this)
    },
    update: function () {
        bg1.y += 2;
        bg2.y += 2;
        if (bg1.y >= 540) { bg1.y = 0 }
        if (bg2.y >= 0) { bg2.y = -bg1.height }
    }
}
states.mainState = {
    create: function () {
        /// ToDo 2: How can we enable physics in Phaser? (Mode : ARCADE)
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.renderer.renderSession.roundPixels = true;
        this.cursor = game.input.keyboard.createCursorKeys();
        bg1 = game.add.image(0, 0, 'background');
        bg2 = game.add.image(0, -bg1.height, 'background');
        this.shoot = game.add.audio('shoot');
        this.music1 = game.add.audio('bg');
        this.music1.play();
        this.player = game.add.sprite(game.width / 2, game.height - 80, 'player');
        game.physics.arcade.enable(this.player);
        this.player.scale.setTo(0.5, 0.5);
        this.helper = game.add.sprite(game.width / 2+60, game.height - 80, 'littlehelper');
        game.physics.arcade.enable(this.helper);
        this.helper.scale.setTo(0.2, 0.2);
        this.player.alive = 5;
        this.helper.alive = true;
        this.bullets = game.add.group();
        this.helperbullets = game.add.group();
        this.enemybullets = game.add.group();
        this.spacebar = this.game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        this.spacebar.onDown.add(function () {
            this.shoot.play();
            this.createBullet();
            if(this.helper.alive)this.createhelperBullet();
        }, this)
        this.bulletSpeed = 5;
        this.galaxingContainer;
        this.galaxings = [];
        this.music = true;
        this.level = [
            {
                map: [
                    2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
                    1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                    0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                ]
            }
        ]
        this.createGalaxing(this.level[0]);
        this.createText();
        this.lifeNumText.text = this.player.alive;
        /// Particle
        game.physics.arcade.enable(this.player);

        this.emitter = game.add.emitter(0, 0, 15);
        this.emitter.makeParticles('pixel');
        this.emitter.setYSpeed(-150, 150);
        this.emitter.setXSpeed(-150, 150);
        this.emitter.setScale(10, 0, 10, 0, 400);
        this.emitter.gravity = 500;

        this.emitter2 = game.add.emitter(0, 0, 15);
        this.emitter2.makeParticles('pixel2');
        this.emitter2.setYSpeed(-150, 150);
        this.emitter2.setXSpeed(-150, 150);
        this.emitter2.setScale(10, 0, 10, 0, 400);
        this.emitter2.gravity = 500;

        this.emitter3 = game.add.emitter(0, 0, 15);
        this.emitter3.makeParticles('pixel3');
        this.emitter3.setYSpeed(-150, 150);
        this.emitter3.setXSpeed(-150, 150);
        this.emitter3.setScale(10, 0, 10, 0, 400);
        this.emitter3.gravity = 500;
        this.heartexist = false;
        var pause_label = game.add.text(game.width - 95, game.height - 50, 'Pause', {
            font: '18px FC',
            fill: '#fff'
        });
        pause_label.anchor = {
            x: 0,
            y: 0
        }
        pause_label.inputEnabled = true;
        pause_label.events.onInputUp.add(function () {
            game.paused = true;
        });
        game.input.onDown.add(unpause, self);
        audioon = game.add.image(game.width - 95, game.height - 90, 'audioon');
        audioon.scale.setTo(0.2, 0.2);
        audioon.inputEnabled = true;
        audioon.anchor = {
            x: 0,
            y: 0
        }
        audioon.events.onInputDown.add(function () {
            if (this.music1.volume <= 0.8) {
                this.music1.volume += 0.2;
                this.shoot.volume += 0.2;
            }
        }, this);
        audiooff = game.add.image(game.width - 55, game.height - 90, 'audiooff');
        audiooff.scale.setTo(0.2, 0.2);
        audiooff.inputEnabled = true;
        audiooff.anchor = {
            x: 0,
            y: 0
        }
        audiooff.events.onInputDown.add(function () {
            if (this.music1.volume >= 0.2) {
                console.log(this.music1.volume)
                this.music1.volume -= 0.2;
                this.shoot.volume -= 0.2;
            }
        }, this);
        // And finally the method that handels the pause menu
        function unpause(event) {
            // Only act if paused
            if (game.paused) {
                if (event.x > game.width - 95 && event.y > game.height - 50) {
                    game.paused = false;
                }
                else {

                }
            }
        };
        game.time.events.loop(Phaser.Timer.SECOND * 5, function () {
            if(this.heartexist == false){
            this.heart = game.add.sprite(game.world.centerX+(Math.random()-0.5)*game.world.centerX/2, game.world.centerY+Math.random()*game.world.centerY, 'heart');
            game.physics.arcade.enable(this.heart);
            this.heart.scale.setTo(0.05, 0.05);
            this.heartexist = true;
            }
        }, this)
    },
    enemykilled: function (gitem, i) {
        gitem.alive = false;
        gitem.isMove = false;
        gitem.isShoot = false;
        this.galaxingContainer.remove(gitem);
        this.galaxings.splice(i, 1);
    },
    hitTestRectangle: function (r1, r2) {
        var hit,
            combinedHalfWidths,
            combinedHalfHeights,
            vx,
            vy

        hit = false;
        r1.middleX = r1.x + r1.parent.x + r1.width / 2;
        r1.middleY = r1.y + r1.parent.y + r1.height / 2;
        r2.middleX = r2.x + r2.parent.x + r2.width / 2;
        r2.middleY = r2.y + r2.parent.y + r2.height / 2;

        r1.halfWidth = r1.width / 2;
        r1.halfHeight = r1.height / 2;
        r2.halfWidth = r2.width / 2;
        r2.halfHeight = r2.height / 2;

        vx = r1.middleX - r2.middleX;
        vy = r1.middleY - r2.middleY;

        combinedHalfWidths = r1.halfWidth + r2.halfWidth;
        combinedHalfHeights = r1.halfHeight + r2.halfHeight;


        if (Math.abs(vx) < combinedHalfWidths) {

            if (Math.abs(vy) < combinedHalfHeights) {
                hit = true;
            } else {
                hit = false;
            }
        } else {
            hit = false;
        }
        return hit;
    },
    update: function () {
        if (!this.player.inWorld) { this.playerDie(); }
        this.movePlayer();
        bg1.y += 2;
        bg2.y += 2;
        if (this.galaxings.length == 0 || this.player.alive <= 0) {
            if (score >= topscore) topscore = score;
            var ex = game.add.sprite(this.player.x, this.player.y, 'explode_player');
            ex.scale.setTo(0.6, 0.6);
            ex.animations.add('explosion', [0, 1, 2, 3])
            ex.animations.play('explosion', 0.00001, false)
            name = " ";
            game.state.start('end');
        }
        if (bg1.y >= 540) { bg1.y = 0 }
        if (bg2.y >= 0) { bg2.y = -bg1.height }
        this.galaxingContainer.x += this.galaxingContainer.vx
        for (var i = 0; i < this.galaxings.length; i++) {
            var gitem = this.galaxings[i];
            if (gitem.x + gitem.parent.x < 0) {
                this.galaxingContainer.vx *= -1
                break
            } else if (gitem.x + gitem.parent.x + gitem.width > game.width) {
                this.galaxingContainer.vx *= -1
                break
            }
        }


        if (this.bullets.children.length) {
            for (var i = 0; i < this.bullets.children.length; i++) {
                var bitem = this.bullets.children[i];
                if (bitem.alive) {
                    if (bitem.y <= -20) {
                        bitem.alive = false;
                        this.bullets.remove(bitem);
                    } else {
                        bitem.y -= this.bulletSpeed;
                    }
                }
            }
        }

        if (this.helperbullets.children.length) {
            for (var i = 0; i < this.helperbullets.children.length; i++) {
                var hitem = this.helperbullets.children[i];
                if (hitem.alive) {
                    if (hitem.y <= -20) {
                        hitem.alive = false;
                        this.helperbullets.remove(hitem);
                    } else {
                        hitem.y -= this.bulletSpeed;
                    }
                }
            }
        }

        if (this.enemybullets.children.length) {
            for (var i = 0; i < this.enemybullets.children.length; i++) {
                var bitem = this.enemybullets.children[i];
                if (bitem.alive) {
                    if (bitem.y >= 540) {
                        bitem.alive = false;
                        this.enemybullets.remove(bitem);
                    } else {
                        bitem.y += this.bulletSpeed;
                    }
                }
            }
        }

        for (var i = 0; i < this.bullets.children.length; i++) {
            var bitem = this.bullets.children[i]
            for (var j = 0; j < this.galaxings.length; j++) {
                var gitem = this.galaxings[j];
                if (bitem.alive && gitem.alive && this.hitTestRectangle(bitem, gitem)) {
                    this.bullets.remove(bitem)
                    bitem.alive = false;
                    if (gitem.color == 'red') {
                        score += 10;
                        console.log('red')
                        this.emitter.x = this.galaxings[j].x + this.galaxings[j].parent.x;
                        this.emitter.y = this.galaxings[j].y + this.galaxings[j].parent.y;
                        this.emitter.start(true, 400, null, 15);
                    } else if (gitem.color == 'blue') {
                        score += 20;
                        console.log('blue')
                        this.emitter2.x = this.galaxings[j].x + this.galaxings[j].parent.x;
                        this.emitter2.y = this.galaxings[j].y + this.galaxings[j].parent.y;
                        this.emitter2.start(true, 400, null, 15);
                    } else if (gitem.color == 'purple') {
                        score += 30;
                        console.log('purple')
                        this.emitter3.x = this.galaxings[j].x + this.galaxings[j].parent.x;
                        this.emitter3.y = this.galaxings[j].y + this.galaxings[j].parent.y;
                        this.emitter3.start(true, 400, null, 15);
                    };
                    this.scoreNumText.text = score;
                    this.enemykilled(this.galaxings[j], j);
                }
            }
        }

        for (var i = 0; i < this.helperbullets.children.length; i++) {
            var bitem = this.helperbullets.children[i]
            for (var j = 0; j < this.galaxings.length; j++) {
                var gitem = this.galaxings[j]
                if (bitem.alive && gitem.alive && this.hitTestRectangle(bitem, gitem)) {
                    this.helperbullets.remove(bitem)
                    bitem.alive = false;
                    if (this.level[0].map[j] == 0) {
                        score += 10;
                        this.emitter.x = this.galaxings[j].x + this.galaxings[j].parent.x;
                        this.emitter.y = this.galaxings[j].y + this.galaxings[j].parent.y;
                        this.emitter.start(true, 400, null, 15);
                    } else if (this.level[0].map[j] == 1) {
                        score += 20;
                        this.emitter2.x = this.galaxings[j].x + this.galaxings[j].parent.x;
                        this.emitter2.y = this.galaxings[j].y + this.galaxings[j].parent.y;
                        this.emitter2.start(true, 400, null, 15);
                    } else if (this.level[0].map[j] == 2) {
                        score += 30;
                        this.emitter3.x = this.galaxings[j].x + this.galaxings[j].parent.x;
                        this.emitter3.y = this.galaxings[j].y + this.galaxings[j].parent.y;
                        this.emitter3.start(true, 400, null, 15);
                    };
                    this.scoreNumText.text = score;
                    this.enemykilled(this.galaxings[j], j);
                }
            }
        }

        for (var i = 0; i < this.enemybullets.children.length; i++) {
            var bitem = this.enemybullets.children[i]
            if (bitem.alive && this.player.alive > 0 && this.hitTestRectangle(bitem, this.player)) {
                this.enemybullets.remove(bitem)
                bitem.alive = false;
                this.player.alive--;
                this.lifeNumText.text = this.player.alive;
            }
            if (bitem.alive && this.helper.alive === true && this.hitTestRectangle(bitem, this.helper)) {
                this.enemybullets.remove(bitem)
                bitem.alive = false;
                this.helper.alive = false;
                this.emitter2.x = this.helper.x;
                this.emitter2.y = this.helper.y;
                this.emitter2.start(true, 400, null, 15);
                this.helper.kill();
            }
        }

        for (var i = 0; i < this.galaxings.length; i++) {
            if (this.galaxings[i].isShoot) {
                this.createEnemyBullet(this.galaxings[i]);
                this.galaxings[i].isShoot = false;
            }
            if (this.galaxings[i].isMove) {
                var a = (this.player.x + this.player.width / 2) - (this.galaxings[i].x + this.galaxings[i].parent.x + this.player.width / 2)

                var b = (this.player.y + this.player.height / 2) - (this.galaxings[i].y + this.galaxings[i].parent.y + this.player.height / 2)

                var c = Math.sqrt(a * a + b * b)

                var speedX = 4 * a / c
                var speedY = 4 * b / c

                this.galaxings[i].x += speedX - this.galaxingContainer.vx
                this.galaxings[i].y += speedY
            }
            if (this.galaxings[i].alive) {
                if (this.hitTestRectangle(this.galaxings[i], this.helper)&&this.helper.alive===true) {
                    this.enemykilled(this.galaxings[i], i);
                    this.helper.alive = false;
                    this.emitter2.x = this.helper.x;
                    this.emitter2.y = this.helper.y;
                    this.emitter2.start(true, 400, null, 15);
                    this.helper.kill();
                }
                if (this.hitTestRectangle(this.galaxings[i], this.player)) {
                    this.enemykilled(this.galaxings[i], i);
                    this.player.alive--;
                    this.lifeNumText.text = this.player.alive;
                    if (this.level[0].map[i] == 0) {
                        this.emitter.x = this.player.x;
                        this.emitter.y = this.player.y;
                        this.emitter.start(true, 400, null, 15);
                    } else if (this.level[0].map[i] == 1) {
                        this.emitter2.x = this.player.x;
                        this.emitter2.y = this.player.y;
                        this.emitter2.start(true, 400, null, 15);
                    } else if (this.level[0].map[i] == 2) {
                        this.emitter3.x = this.player.x;
                        this.emitter3.y = this.player.y;
                        this.emitter3.start(true, 400, null, 15);
                    };
                }
            }
        }
        game.physics.arcade.overlap(this.player, this.heart,this.addlife, null, this);
    },
    createBullet: function () {
        var bullet = this.bullets.getFirstExists(false)
        if (bullet) {
            bullet.reset(this.player.body.x + 16, this.player.body.y - 20)
            bullet.alive = true;
        } else {
            bullet = game.add.sprite(this.player.body.x + 27, this.player.body.y - 15, 'playershot');
            game.physics.arcade.enable(bullet);
            bullet.scale.setTo(4.0, 4.0);
            bullet.alive = true;
            this.bullets.addChild(bullet);
        }
    },
    createhelperBullet: function () {
        var helperbullet = this.helperbullets.getFirstExists(false)
        if (helperbullet) {
            helperbullet.reset(this.helper.body.x + 16, this.helper.body.y - 20)
            helperbullet.alive = true;
        } else {
            helperbullet = game.add.sprite(this.helper.body.x + 27, this.helper.body.y - 15, 'littleshot');
            game.physics.arcade.enable(helperbullet);
            helperbullet.scale.setTo(4.0, 4.0);
            helperbullet.alive = true;
            this.helperbullets.addChild(helperbullet);
        }
    },
    addlife: function() {
        this.player.alive++;
        this.lifeNumText.text = this.player.alive;
        this.heart.kill();
        this.heartexist = false;
    },
    createEnemyBullet: function (gitem) {
        var bullet = this.enemybullets.getFirstExists(false)
        if (bullet) {
            bullet.reset(gitem.x + gitem.parent.x + 21, gitem.y + gitem.parent.y + 30)
            bullet.alive = true;
        } else {
            bullet = game.add.sprite(gitem.x + gitem.parent.x + 21, gitem.y + gitem.parent.y + 30, 'enemyshot');
            game.physics.arcade.enable(bullet);
            bullet.scale.setTo(4.0, 4.0);
            bullet.alive = true;
            this.enemybullets.addChild(bullet);
        }
    },
    playerDie: function () { game.state.start('end'); },
    createText: function () {
        this.scoreText = game.add.text(20, 500, 'SCORE:', {
            font: '16px FC',
            align: 'center',
            fill: '#ff2600'
        })
        this.scoreNumText = game.add.text(120, 500, '0', {
            font: '16px FC',
            align: 'center',
            fill: '#ffffff'
        })
        this.lifeText = game.add.text(20, 470, 'LIFE:', {
            font: '16px FC',
            align: 'center',
            fill: '#ffff33'
        })
        this.lifeNumText = game.add.text(120, 470, '0', {
            font: '16px FC',
            align: 'center',
            fill: '#ffffff'
        })
    },
    createGalaxing: function (level) {
        this.galaxingContainer = game.add.group()
        for (var i = 0; i < level.map.length; i++) {
            if (level.map[i] == 0) {
                var temp = game.add.sprite((i % 10) * 80, Math.floor(i / 10) * 50, 'enemy1');
                temp.color = 'red';
            } else if (level.map[i] == 1) {
                var temp = game.add.sprite((i % 10) * 80, Math.floor(i / 10) * 50, 'enemy2');
                temp.color = 'blue';
            } else if (level.map[i] == 2) {
                var temp = game.add.sprite((i % 10) * 80, Math.floor(i / 10) * 50, 'enemy3');
                temp.color = 'purple';
            };
            game.physics.arcade.enable(temp);
            temp.scale.setTo(0.2, 0.2);
            temp.alive = true;
            if (level.map[i] == 0) {
                if (i % 2 == 0) {
                    temp.animations.add('fly', [0, 1])
                } else {
                    temp.animations.add('fly', [1, 0])
                }
            } else if (level.map[i] == 1) {
                if (i % 2 == 0) {
                    temp.animations.add('fly', [0, 1])
                } else {
                    temp.animations.add('fly', [1, 0])
                }
            } else {
                if (i % 2 == 0) {
                    temp.animations.add('fly', [0, 1])
                } else {
                    temp.animations.add('fly', [1, 0])
                }
            }
            temp.animations.play('fly', 2, true)
            this.galaxings.push(temp)
            this.galaxingContainer.add(temp)
        }

        this.galaxingContainer.y = 20
        this.galaxingContainer.x = game.world.centerX - this.galaxingContainer.width / 2
        this.galaxingContainer.vx = 2

        game.time.events.loop(Phaser.Timer.SECOND * 2.5, function () {
            var now = this.galaxings[Math.floor(Math.random() * this.galaxings.length)];
            now.isMove = true;
        }, this)
        game.time.events.loop(Phaser.Timer.SECOND * 0.5, function () {
            var now = this.galaxings[Math.floor(Math.random() * this.galaxings.length)];
            now.isShoot = true;
        }, this)
    },
    movePlayer: function () {
        if (this.cursor.left.isDown) {
            if (this.player.body.x >= 0) {
                this.player.body.velocity.x = -200;
                this.helper.body.velocity.x = -200;
            } else {
                this.player.body.velocity.x = 0;
                this.helper.body.velocity.x = 0;
            }
            this.player.body.velocity.y = 0;
            this.helper.body.velocity.y = 0;
        }
        else if (this.cursor.right.isDown) {
            if (this.player.body.x + 40 <= 900) {
                this.player.body.velocity.x = 200;
                this.helper.body.velocity.x = 200;
            } else {
                this.player.body.velocity.x = 0;
                this.helper.body.velocity.x = 0;
            }
            this.player.body.velocity.y = 0;
            this.helper.body.velocity.y = 0;
        }
        // If the up arrow key is pressed, And the player is on the ground.
        else if (this.cursor.up.isDown) {
            if (this.player.body.y - 10 >= 0) {
                this.player.body.velocity.y = -200;
                this.helper.body.velocity.y = -200;
            } else {
                this.player.body.velocity.y = 0;
                this.helper.body.velocity.y = 0;
            }
            this.player.body.velocity.x = 0;
            this.helper.body.velocity.x = 0;
        }
        // If neither the right or left arrow key is pressed
        else if (this.cursor.down.isDown) {
            if (this.player.body.y + 50 <= 540) {
                this.player.body.velocity.y = 200;
                this.helper.body.velocity.y = 200;
            } else {
                this.player.body.velocity.y = 0;
                this.helper.body.velocity.y = 0;
            }
            this.player.body.velocity.x = 0;
            this.helper.body.velocity.x = 0;
        }
        else {
            this.player.body.velocity.x = 0;
            this.player.body.velocity.y = 0;
            this.helper.body.velocity.x = 0;
            this.helper.body.velocity.y = 0;
        }
    }
};

var game = new Phaser.Game(900, 540, Phaser.AUTO, 'canvas');
game.state.add('main', states.mainState);
game.state.add('start', states.start);
game.state.add('end', states.end);
game.state.add('pause', states.pause);
game.state.start('start');



