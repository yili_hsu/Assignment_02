# Assignment2
## Topic
* Project Name : [Assignment2]
* Key functions
    1. 遊戲初始畫面：玩家點擊TAP TO START之後就會進入遊戲畫面
    2. 遊戲畫面：
        (1)點擊上下左右可以操控戰機，空白鍵可以發射子彈。
        (2)敵機會隨機進行自殺式攻擊衝向玩家，也會不定時發射子彈攻擊玩家。
        (3)左下角顯示玩家剩餘生命和分數（紅色10分、藍綠色20分、紫色30分）
        (4)右下角可以暫停遊戲，調整音量大小（左邊為放大音量、右邊為減小音量）
    3. 結束畫面：
        (1)點擊TAP TO RESTART之後可以重新開始遊戲
        (2)TOP SCORE顯示firebase中分數最高的玩家名稱和分數，YOUR SCORE則顯示該局玩家名稱和分數
        (3)玩家可以透過畫面中的鍵盤輸入名稱，點擊ok之後就會將名稱和分數上傳到firebase並且更新TOP SCORE
    
* Other functions
    1. 敵機左右移動時有動畫
    2. 遊戲進行中有背景音樂、發射子彈時也會有音效
    3. 敵機撞上玩家或者被子彈射中時會有particle的爆炸特效
    4. 背景會隨遊戲進行而滾動

## Basic Components
|Component|Score|Y/N|
|Complete game process|15%|Y|
|Basic rules|20%|Y|
|Jucify mechanisms|15%|Y|
|Animations / Particle Systems|each for 10%|Y|
|UI / Sound effects / Leaderboard|each for 5%|Y|
|Appearance (subjective)|5%|Y|

## Advanced Components
|Bonus (describe on README.md)|40%|
# 作品網址：https://105070026.gitlab.io/Assignment_02

# Components Description : 
1. Complete game process: 遊戲初始畫面=>遊戲畫面=>終止畫面（restart/score board）
2. Basic rules :
    (1) 玩家可透過鍵盤的上下左右自由移動（但不可超出遊戲畫面）和 空白鍵射擊
    (2) 當玩家被敵機的子彈射中或者撞到敵機時生命值-1
3. Jucify mechanisms :
    (1) 敵機會隨機進行自殺式攻擊衝向玩家，也會不定時發射子彈攻擊玩家。
4. Animations / Particle Systems : 
    (1) 敵機左右移動時會有動畫
    (2) 背景隨遊戲進行滾動
    (3) 當敵機被玩家子彈射中或者撞擊玩家時會有particle爆炸特效
5. UI:
    (1) Player health:畫面左下角顯示剩餘life
    (2) Score: 畫面左下角顯示目前分數（紅色敵機10分、藍綠色敵機20分、紫色敵機30分）
    (3) volume control: 右下角可以暫停遊戲，調整音量大小（左邊為放大音量、右邊為減小音量）
6. Sound effects
    (1) 遊戲進行時有背景音樂
    (2) 玩家射擊子彈時有音效
7. Leaderboard
    (1) 遊戲結束畫面中TOP SCORE顯示firebase中分數最高的玩家名稱和分數，YOUR SCORE則顯示該局玩家名稱和分數
    (2) (3)玩家可以透過畫面中的鍵盤輸入名稱，點擊ok之後就會將名稱和分數上傳到firebase並且更新TOP SCORE
8. Appearance (subjective)
    (1) 美美的字體和遊戲畫面!
9. BONUS
    (1) 敵機會隨機進行自殺式攻擊衝向玩家!